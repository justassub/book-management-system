package lt.sda.academy.data;

import lombok.RequiredArgsConstructor;
import lt.sda.academy.model.Author;
import lt.sda.academy.presentation.AuthorChoice;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class AuthorService {
    private final SessionFactory sessionFactory;

    public void createAuthor(Author author) {

        boolean isValid = AuthorValidator.validateAuthor(author);

        if (isValid) {
            Session session = sessionFactory.openSession();
            session.save(author);
            AuthorChoice.authorWasSavedMessage(author);
            session.close();
        }
    }

    public List<Author> getAllAuthors() {
        return sessionFactory.openSession().createQuery("from Author", Author.class).list();
    }

    public Optional<Author> getAuthorById(Long id) {
        return Optional.ofNullable(sessionFactory.openSession().find(Author.class, id));
    }

    public List<Author> searchAuthorByNameOrSurname(String name) {
        Session session = sessionFactory.openSession();
        TypedQuery<Author> authorQuery = session.createQuery("select a from Author a where " +
                        "lower(a.familyName) LIKE lower(:searchKeyword) or " +
                        "lower(a.name) LIKE lower(:searchKeyword)",
                Author.class);
        authorQuery.setParameter("searchKeyword", "%" + name + "%");
        return authorQuery.getResultList();
    }

    public void updateAuthor(Author author) {
        boolean isValid = AuthorValidator.validateAuthor(author);
        if (isValid) {
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            session.update(author);
            AuthorChoice.authorWasUpdatedMessage(author);
            session.flush();
        }
    }

    public void deleteAuthor(Author author) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(author);
        session.flush();
        AuthorChoice.authorWasDeletedSuccessfully(author);
    }
}
