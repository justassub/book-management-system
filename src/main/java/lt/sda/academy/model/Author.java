package lt.sda.academy.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String familyName;

    public Author(String name, String familyName) {
        this.name = name;
        this.familyName = familyName;
    }

    public String toStringNameAndFamilyName() {
        return makeString(name, familyName);
    }

    public String toStringIdNameAndFamilyName() {
        return makeString(id.toString(), name, familyName);
    }

    public String makeString(String... fields) {
        return String.join(", ", fields);
    }
}
