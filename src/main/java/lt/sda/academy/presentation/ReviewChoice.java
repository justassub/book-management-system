package lt.sda.academy.presentation;

import lt.sda.academy.util.ScannerUtil;

public class ReviewChoice {
    public static int selectReviewChoice() {
        System.out.println("1.Matyti visus knygų ivertinimus");
        System.out.println("2.Matyti įvertinimas knygai pagal id");
        System.out.println("3.Pridėti naują įvertinimą knygai.");
        return ScannerUtil.SCANNER.nextInt();
    }
}
